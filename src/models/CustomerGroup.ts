import BaseModel from "./BaseModel";
import { ListConfig } from "../core/dataGet";

export default class CustomerGroup extends BaseModel{
    id!: number;
    name!: string;
    constructor(row?:any){
        super('ta_customer_groups');
        if(row!==undefined){
            this.id=row.id;
            this.name=row.name;
            this.exists=true;
        }
    }
    /**
     * Buscar por id un CustomerGroup
     * @param id 
     */
    public findById(id:number):Promise<CustomerGroup>{
        return new Promise(async (resolve, reject)=>{
            let customer=new CustomerGroup();
            let sql=`SELECT 
                *
                from ${this.getTable()}
                where 
                id=${this.getDB().escape(id)}`;
            let results=await this.getDB().query(sql);
            if(results.length==1){
                let data=results[0];
                customer=new CustomerGroup(data);
            }
            resolve(customer);
        });
        
    }
    /**
     * insertar o actualizar CustomerGroup
     */
    public insert(){
        return new Promise(async (resolve, reject)=>{
            if(this.exists){
                let sql=`UPDATE ${this.getTable()}
                        SET
                        name=${this.getDB().escape(this.name)}`;
                sql+=` WHERE id=${this.getDB().escape(this.id)}`;
                try {
                    await this.getDB().query(sql);  
                    resolve(true);
                } catch(e) {
                    resolve(false);
                }
                
            }else{
                let sql=`INSERT INTO ${this.getTable()} (
                    name
                    )
                     VALUES (
                    ${this.getDB().escape(this.name)}`;
                    sql+=`)`;
                try {
                    let results=await this.getDB().query(sql); 
                    this.id=results.insertId;
                    this.exists=true;
                    resolve(true);
                } catch(e) {
                    resolve(false);
                }
            }
        });
    }
    /**
     * Eliminar CustomerGroup
     */
    public delete(){
        return new Promise(async (resolve, reject)=>{
            if(this.exists){
                let sql=`UPDATE ta_customers
                        SET
                        id_customer_group=0 WHERE id_customer_group=${this.getDB().escape(this.id)}`;
                await this.getDB().query(sql); 
                 
                sql=`DELETE FROM ${this.getTable()} WHERE id=${this.getDB().escape(this.id)}`;
                try {
                    await this.getDB().query(sql);  
                    resolve(true);
                } catch(e) {
                    resolve(false);
                }
            }else{
                resolve(false);
            }
        });
    }


    public getCustomerGroups(list_config?:ListConfig):Promise<{
        total:number, 
        elements:CustomerGroup[], 
        page:number, 
        limit:number
    }>{
        return new Promise(async (resolve, reject)=>{
            let total:number=0;
            let page=0;
            let limit=0;
            let elements:CustomerGroup[]=[];
            let customerGroup=new CustomerGroup();
            let sql_count="select count(id) as total ";
            let sql_rows="select * ";
            let sql=` from ${this.getTable()} 
            where 1=1 `;

            if(list_config!==undefined){
                sql+=await customerGroup.filtersSql(list_config.filters);
                sql_count+=sql;
                sql+=` and name like ${this.getDB().escape('%'+list_config.search+'%')} `;
                if(list_config.sort.length>0)sql+=await customerGroup.sortSql(list_config.sort);
                else sql+=` ORDER BY name ASC `;
                sql+=customerGroup.limitSql(list_config);
                page=list_config.page;
                limit=list_config.limit;
            }else{
                sql_count+=sql;
                sql+=` ORDER BY name ASC `;
            }
            sql_rows+=sql;
            let results=await this.getDB().query(sql_count);
            total=results[0].total;

            results=await this.getDB().query(sql_rows);

            for(let data of results){
                elements.push(new CustomerGroup(data));
            }

            resolve({
                total,
                elements,
                page,
                limit
            });
        });


    }
    
}