import MySQL from "../core/mysql";
import DataGet, { FieldsConfig, ListConfigSort, ListConfig } from "../core/dataGet";
import { isArray } from "util";

export default class BaseModel{
    private db:MySQL;
    protected exists:boolean=false;
    private privateColumns:string[]=[];
    private columns:string[]=[];
    private embed:any={};
    private table:string;
    protected type_element_images:number=0;
    constructor(table:string){
        this.table=table;
        this.db=MySQL.instance;
    }
    
    protected getTable(){
        return this.table;
    }

    private initializeColumns(){
        return new Promise((resolve, reject)=>{
            let sql=`SHOW COLUMNS FROM ${this.table}`;
            this.db.query(sql).then(results=>{
                for (let data of results) {
                    let index=this.privateColumns.indexOf(data['Field']);
                    if(index==-1){
                        this.columns.push(data['Field']);
                    }
                }
                resolve(true);
            }).catch(()=>{
                reject(false);
            });
        });
    }


    private getColumns():Promise<string[]>{
        return new Promise((resolve, reject)=>{
            if(this.columns.length==0){
                this.initializeColumns().then(()=>{
                    resolve(this.columns);
                }).catch(()=>{
                    reject(false);
                });
            }else{
                resolve(this.columns);
            }
        });
    }


    protected getDB(){
        return this.db;
    }

    public getExists(){
        return this.exists;
    }

    protected setPrivateColumn(column:string){
        this.privateColumns.push(column);
    }

    protected setEmbed(embed:string, method:string){
        this.embed[embed]=method;
    }

    public toObject(fields_config?:FieldsConfig):Promise<any>{
        return new Promise((resolve, reject)=>{
            let object:any={};
            let objectThis:any=this;
            this.getColumns().then(async columns=>{
                for(let column of columns){
                    object[column]=objectThis[column];
                }
                if(fields_config && fields_config.embed.length>0){
                    for(let embed of fields_config.embed){
                        if(this.embed[embed.embed]!==undefined){
                            let dataGET=new DataGet();
                            let result= await objectThis[this.embed[embed.embed]]();
                            
                            if(isArray(result)){
                                object[embed.embed]=[];
                                for(let i=0; i<result.length;i++){
                                    let objResult=await result[i].toObject(dataGET.fields_config);
                                    object[embed.embed].push(objResult);
                                }
                            }else if(typeof result === "object"){
                                if(result.total!==undefined && result.elements!==undefined ){
                                    object[embed.embed]=[];
                                    for(let i=0; i<result.elements.length;i++){
                                        let objResult=await result.elements[i].toObject(dataGET.fields_config);
                                        object[embed.embed].push(objResult);
                                    }
                                }else{
                                    let objResult=await result.toObject(dataGET.fields_config);
                                    object[embed.embed]=objResult;
                                }
                            }else{
                                object[embed.embed]=result;
                            }
                        }
                    }
                }
                resolve(object);
            }).catch(()=>{
                reject(false);
            });
        });
        
        
    }

    public filtersSql(filters:any, abbreviationTable:string=''){
        return new Promise((resolve, reject)=>{
            if(abbreviationTable!='')abbreviationTable+='.';
            let sql:string='';
            this.getColumns().then(columns=>{
                for (const prop in filters) {
                    let indexColumn=columns.indexOf(prop);
                    if(indexColumn!=-1){
                        sql+=` and ${abbreviationTable}${columns[indexColumn]}=${this.db.escape(filters[prop])} `;
                    }
                }
                resolve(sql);
            }).catch(()=>{
                reject(false);
            });;
        });
    }


    public sortSql(sorts:ListConfigSort[], abbreviationTable:string=''):Promise<string>{
        return new Promise((resolve, reject)=>{
            if(abbreviationTable!='')abbreviationTable+='.';
            let sql:string='';
            this.getColumns().then(columns=>{
                for(let sort of sorts){
                    let indexColumn=columns.indexOf(sort.column);
                    if(indexColumn!=-1){
                        switch(sort.direction){
                            case 'ASC':
                                if(sql!='')sql+=', ';
                                sql=`${sql}${abbreviationTable}${columns[indexColumn]} ASC`;
                                break;
                            case 'DESC':
                                if(sql!='')sql+=', ' ;
                                sql=`${sql}${abbreviationTable}${columns[indexColumn]} ASC`;
                                break;
                        }
                    }
                }
                if(sql!=''){
                    sql=` ORDER BY ${sql} `;
                }
                resolve(sql);
            }).catch(()=>{
                reject(false);
            });;
        });
    }

    public limitSql(config:ListConfig){
        let sql='';
        if(config.not_limit==0){
            let offset=config.page*config.limit;
            sql=`LIMIT ${this.db.escape(offset)}, ${this.db.escape(config.limit)}`;
        }
        return sql;
    }

    
    
    

}