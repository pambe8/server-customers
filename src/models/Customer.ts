import BaseModel from "./BaseModel";
import { ListConfig } from "../core/dataGet";
import CustomerGroup from "./CustomerGroup";

export default class Customer extends BaseModel{
    id!: number;
    name!: string;
    description!: string;
    id_customer_group!:number;
    constructor(row?:any){
        super('ta_customers');
        this.setEmbed('customer_group', 'getCustomerGroup');
        if(row!==undefined){
            this.id=row.id;
            this.name=row.name;
            this.description=row.description;
            this.id_customer_group=row.id_customer_group;
            this.exists=true;
        }
    }
    /**
     * Buscar por id un Customer
     * @param id 
     */
    public findById(id:number):Promise<Customer>{
        return new Promise(async (resolve, reject)=>{
            let customer=new Customer();
            let sql=`SELECT 
                *
                from ${this.getTable()}
                where 
                id=${this.getDB().escape(id)}`;
            let results=await this.getDB().query(sql);
            if(results.length==1){
                let data=results[0];
                customer=new Customer(data);
            }
            resolve(customer);
        });
        
    }
    /**
     * Insertar o actualizar Customer
     */
    public insert(){
        return new Promise(async (resolve, reject)=>{
            if(this.exists){
                let sql=`UPDATE ${this.getTable()}
                        SET
                        name=${this.getDB().escape(this.name)},
                        description=${this.getDB().escape(this.description)},
                        id_customer_group=${this.getDB().escape(this.id_customer_group)}`;
                sql+=` WHERE id=${this.getDB().escape(this.id)}`;
                try {
                    await this.getDB().query(sql);  
                    resolve(true);
                } catch(e) {
                    resolve(false);
                }
                
            }else{
                let sql=`INSERT INTO ${this.getTable()} (
                    name,
                    description,
                    id_customer_group
                    )
                     VALUES (
                    ${this.getDB().escape(this.name)},
                    ${this.getDB().escape(this.description)},
                    ${this.getDB().escape(this.id_customer_group)}`;
                    sql+=`)`;
                try {
                    let results=await this.getDB().query(sql); 
                    this.id=results.insertId;
                    this.exists=true;
                    resolve(true);
                } catch(e) {
                    resolve(false);
                }
            }
        });
    }
    /**
     * Eliminar Customer
     */
    public delete(){
        return new Promise(async (resolve, reject)=>{
            if(this.exists){
                let sql=`DELETE FROM ${this.getTable()} WHERE id=${this.getDB().escape(this.id)}`;
                try {
                    await this.getDB().query(sql);  
                    resolve(true);
                } catch(e) {
                    resolve(false);
                }
            }else{
                resolve(false);
            }
        });
    }
    /**
     * Consultar CustomerGroup del Customer
     */
    public async getCustomerGroup():Promise<CustomerGroup>{
        let customerGroup=new CustomerGroup();
        customerGroup=await customerGroup.findById(this.id_customer_group);
        return customerGroup;
    }

    /**
     * Listado de Customers
     * @param list_config 
     */
    public getCustomers(list_config?:ListConfig):Promise<{
        total:number, 
        elements:Customer[], 
        page:number, 
        limit:number
    }>{
        return new Promise(async (resolve, reject)=>{
            let total:number=0;
            let page=0;
            let limit=0;
            let elements:Customer[]=[];
            let customer=new Customer();
            let sql_count="select count(id) as total ";
            let sql_rows="select * ";
            let sql=` from ${this.getTable()} 
            where 1=1 `;

            if(list_config!==undefined){
                sql+=await customer.filtersSql(list_config.filters);
                sql_count+=sql;
                sql+=` and name like ${this.getDB().escape('%'+list_config.search+'%')} `;
                if(list_config.sort.length>0)sql+=await customer.sortSql(list_config.sort);
                else sql+=` ORDER BY name ASC `;
                sql+=customer.limitSql(list_config);
                page=list_config.page;
                limit=list_config.limit;
            }else{
                sql_count+=sql;
                sql+=` ORDER BY name ASC `;
            }
            sql_rows+=sql;
            let results=await this.getDB().query(sql_count);
            total=results[0].total;

            results=await this.getDB().query(sql_rows);

            for(let data of results){
                elements.push(new Customer(data));
            }

            resolve({
                total,
                elements,
                page,
                limit
            });
        });


    }

}