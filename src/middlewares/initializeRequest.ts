import {Request, Response, NextFunction} from 'express';
import config from '../config/config';
import DataGet, { FieldsConfigEmbed, ListConfigSort } from '../core/dataGet';

export default class initializeRequest{

	/**
	 * inicializamos petición
	 */
  	public static start=(req:Request, res:Response, next:NextFunction)=>{
        let dataGET=initializeRequest.readDataGET(req.query);
		req.query.dataGET=dataGET;
		next();
	}

	/**
	 * Leer parámetros GET para generar estructura DataGet
	 * @param query 
	 */
    private static readDataGET(query:any){
		let dataGET=new DataGet();
		//Recorremos cada parámetro GET
        for (let index in query) {
			    let value=query[index];
				switch (index){
					case 'embed':
						if(value!=''){
							let embeds=value.split(",");
							for (let embed of embeds) {
								let index=dataGET.fields_config.embed.length;
								dataGET.fields_config.embed[index]=new FieldsConfigEmbed();
								dataGET.fields_config.embed[index]['embed']=embed;
							}
						}
						break;
					case 'sort':
						if(value!=''){
							let columns=value.split(",");
							for (let column of columns) {
								if(dataGET.list_config.sort==null)dataGET.list_config.sort=[];
								let car=column.substring(0, 1)
								if(car=='-'){
									column=column.replace(car, "");
									dataGET.list_config.sort.push(new ListConfigSort(column, 'DESC'));
								}else{
									dataGET.list_config.sort.push(new ListConfigSort(column, 'ASC'));
								}
							}
							
						}
						break;
					case 'search':
						dataGET.list_config.search=value;
						break;
					case 'page':
						dataGET.list_config.page=parseInt(value);
						break;
					case 'limit':
						let limit=parseInt(value);
						if(limit>config.MAX_LIMIT)limit=config.MAX_LIMIT;
						dataGET.list_config.limit=limit;
						break;
					case 'not_limit':
						let not_limit=parseInt(value);
						if(not_limit!=0 && not_limit!=1)not_limit=0;
						dataGET.list_config.not_limit=not_limit;
						break;
					default:
						dataGET.list_config.filters[index]=value;
						break;
				}
		}
		
		if(dataGET.list_config.not_limit==1){
			dataGET.list_config.limit=-1;
		}
        
        return dataGET;
	}
}




