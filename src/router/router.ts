import {Router} from 'express';
import initializeRequest from '../middlewares/initializeRequest';
import routerCustomers from './customers';
import routerCustomerGroups from './customer_groups';
const router = Router();

router.use('/', initializeRequest.start, routerCustomers);
router.use('/', initializeRequest.start, routerCustomerGroups);

export default router;