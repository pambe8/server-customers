import {Router} from 'express';
import CustomersController from '../controllers/CustomersController';


const routerCustomers = Router();
routerCustomers.get('/customers', CustomersController.getCustomers);
routerCustomers.post('/customers', CustomersController.postCustomer);
routerCustomers.get('/customers/:id', CustomersController.getCustomer);
routerCustomers.put('/customers/:id', CustomersController.putCustomer);
routerCustomers.delete('/customers/:id', CustomersController.deleteCustomer);

export default routerCustomers;