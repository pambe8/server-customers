import {Router} from 'express';
import CustomerGroupsController from '../controllers/CustomerGroupsController';


const routerCustomerGroups = Router();
routerCustomerGroups.get('/customer-groups', CustomerGroupsController.getCustomerGroups);
routerCustomerGroups.post('/customer-groups', CustomerGroupsController.postCustomerGroup);
routerCustomerGroups.get('/customer-groups/:id', CustomerGroupsController.getCustomerGroup);
routerCustomerGroups.put('/customer-groups/:id', CustomerGroupsController.putCustomerGroup);
routerCustomerGroups.delete('/customer-groups/:id', CustomerGroupsController.deleteCustomerGroup);

export default routerCustomerGroups;