import express = require('express');
import http from 'http';
import MySQL from '../core/mysql';
import config from '../config/config';
export default class Server{
    private static _instance:Server;//propiedad de la instancia (Singleton)
    public app:express.Application;
    public port:number;
    private httpServer:http.Server;
    constructor(){
        this.port=config.port;
        this.app=express();
        this.httpServer=new http.Server(this.app);
    }
    /**
     * devolver instancia (Singleton)
     */
    public static get instance(){
        return this._instance || (this._instance=new this());
    }
    /**
     * iniciar httpServer en el puerto indicado
     * @param callback 
     */
    start(callback:any){
        MySQL.instance;
        this.httpServer.listen(this.port, callback);
    }
}