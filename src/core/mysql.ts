import mysql = require('mysql');
import config from '../config/config';
const xss = require("xss");
export default class MySQL{
    private static _instance: MySQL; //propiedad de la instancia (Singleton)
    private cnn:mysql.Pool;
    constructor(){
        this.cnn=mysql.createPool({
            connectionLimit : config.connection_db.connection_limit,
            host:config.connection_db.host,
            user:config.connection_db.user,
            password:config.connection_db.password,
            database:config.connection_db.database,
            dateStrings: true,
            charset: config.connection_db.charset
        });
        this.cnn.getConnection((err, connection) => {
            if (err) {
                if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                    console.error('Database connection was closed.');
                }
                if (err.code === 'ER_CON_COUNT_ERROR') {
                    console.error('Database has too many connections.');
                }
                if (err.code === 'ECONNREFUSED') {
                    console.error('Database connection was refused.');
                }
            }
            if (connection) connection.release();
            return
        });
    }

    /**
     * Limpiar cadena para poder ser insertada en consulta
     * @param data 
     */
    public escape(data:any){
        data=this.cnn.escape(data);
        data=xss(data);
        return data;
    }

    /**
     * devolver instancia (Singleton)
     */
    public static get instance(){ 
        return this._instance || (this._instance=new this());
    }

    /**
     * realizar consulta
     * @param query 
     */
    public query(query:string):Promise<any[] | any>{
        return new Promise((resolve, reject)=>{
            this.cnn.query(query, (error, results:Object[], fields)=>{
                if(error){
                    console.log(error);
                    reject(error);
                }else{
                    resolve(results);
                }
            });
        });
    }
}