import config from "../config/config";

/**
 * Estructura de configuración de petición según parámetros GET
 */
export default class DataGet{
    fields_config:FieldsConfig;
    list_config:ListConfig;
    constructor(){
        this.fields_config=new FieldsConfig();
        this.list_config=new ListConfig();
    }
}
/**
 * Configuración de listado
 */
export class ListConfig{
    filters:any={};
    sort:ListConfigSort[]=[];
    search:string='';
    page:number=0;
    limit:number=config.MAX_LIMIT;
    not_limit:number=0;
}
/**
 * configuración de orden del listado
 */
export class ListConfigSort{
    column:string;
    direction:string;
    constructor(column:string, direction:string){
        this.column=column;
        this.direction=direction;
    }
}
/**
 * configuración sobre campos del objeto
 */
export class FieldsConfig{
    embed:FieldsConfigEmbed[]=[];
}

export class FieldsConfigEmbed{
    embed:string='';
}