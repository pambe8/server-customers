import {Request, Response} from 'express';
import DataGet from '../core/dataGet';
import Customer from '../models/Customer';


export default class CustomersController{
    /**
     * listado de Customers
     */
    public static getCustomers=async (req:Request, res:Response)=>{
        let dataGET:DataGet=req.query.dataGET;
        let objCustomer=new Customer();
        let result=await objCustomer.getCustomers(dataGET.list_config);
        let data:any[]=[];
        for(let element of result.elements){
            data.push(await element.toObject(dataGET.fields_config));
        }
        res.json({
            status:true,
            data,
            total:result.total,
            page:result.page,
            limit:result.limit
        });
    }
    /**
     * Consultar Customer
     */
    public static getCustomer=async (req:Request, res:Response)=>{
        let dataGET:DataGet=req.query.dataGET;
        let id_customer:number=parseInt(req.params.id);
        let objCustomer=new Customer();
        objCustomer=await objCustomer.findById(id_customer);
        if(objCustomer.getExists()){
            let element=await objCustomer.toObject(dataGET.fields_config);
            res.json({
                status:true,
                element
            });
        }else{
            res.json({
                status:false,
                error: 'not exists'
            });
        }
    }
    /**
     * Crear Customer
     */
    public static postCustomer=async (req:Request, res:Response)=>{
        let name=req.body.name;
        let description=req.body.description;
        let id_customer_group=req.body.id_customer_group;
        let objCustomer=new Customer();
        objCustomer.name=name;
        objCustomer.description=description;
        objCustomer.id_customer_group=id_customer_group;
        objCustomer.insert().then(async status=>{
            if(status){
                let element=await objCustomer.toObject();
                res.json({
                    status:true,
                    element
                });
            }else{
                res.json({
                    status:false,
                    error: 'Error'
                });
            }
        });
    }
    /**
     * Actualizar Customer
     */
    public static putCustomer=async (req:Request, res:Response)=>{
        let name=req.body.name;
        let description=req.body.description;
        let id_customer_group=req.body.id_customer_group;
        let id_customer=parseInt(req.params.id);
        let objCustomer=new Customer();
        objCustomer=await objCustomer.findById(id_customer);
        if(objCustomer.getExists()){
            objCustomer.name=name;
            objCustomer.description=description;
            objCustomer.id_customer_group=id_customer_group;
            objCustomer.insert().then(async status=>{
                if(status){
                    let element=await objCustomer.toObject();
                    res.json({
                        status:true,
                        element
                    });
                }else{
                    res.json({
                        status:false,
                        error: 'Error'
                    });
                }
            });
        }else{
            res.json({
                status:false,
                error: 'not exists'
            });
        }
    }
    /**
     * Eliminar Customer
     */
    public static deleteCustomer=async (req:Request, res:Response)=>{
        let id_customer=parseInt(req.params.id);
        let objCustomer=new Customer();
        objCustomer=await objCustomer.findById(id_customer);
        if(objCustomer.getExists()){
            objCustomer.delete().then(async status=>{
                if(status){
                    res.json({
                        status:true
                    });
                }else{
                    res.json({
                        status:false,
                        error: 'Error'
                    });
                }
            });
        }else{
            res.json({
                status:false,
                error: 'not exists'
            });
        }
    }
}