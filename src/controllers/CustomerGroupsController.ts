import {Request, Response} from 'express';
import DataGet from '../core/dataGet';
import CustomerGroup from '../models/CustomerGroup';

export default class CustomerGroupsController{
    /**
     * Listado de CustomerGroup
     */
    public static getCustomerGroups=async (req:Request, res:Response)=>{
        let dataGET:DataGet=req.query.dataGET;
        let objCustomerGroup=new CustomerGroup();
        let result=await objCustomerGroup.getCustomerGroups(dataGET.list_config);
        let data:any[]=[];
        for(let element of result.elements){
            data.push(await element.toObject(dataGET.fields_config));
        }
        res.json({
            status:true,
            data,
            total:result.total,
            page:result.page,
            limit:result.limit
        });
    }
    /**
     * Consultar CustomerGroup
     */
    public static getCustomerGroup=async (req:Request, res:Response)=>{
        let dataGET:DataGet=req.query.dataGET;
        let id_customer_group:number=parseInt(req.params.id);
        let objCustomerGroup=new CustomerGroup();
        objCustomerGroup=await objCustomerGroup.findById(id_customer_group);
        if(objCustomerGroup.getExists()){
            let element=await objCustomerGroup.toObject(dataGET.fields_config);
            res.json({
                status:true,
                element
            });
        }else{
            res.json({
                status:false,
                error: 'not exists'
            });
        }
    }
    /**
     * Crear CustomerGroup
     */
    public static postCustomerGroup=async (req:Request, res:Response)=>{
        let name=req.body.name;
        let objCustomerGroup=new CustomerGroup();
        objCustomerGroup.name=name;
        objCustomerGroup.insert().then(async status=>{
            if(status){
                let element=await objCustomerGroup.toObject();
                res.json({
                    status:true,
                    element
                });
            }else{
                res.json({
                    status:false,
                    error: 'Error'
                });
            }
        });
    }
    /**
     * Actualizar CustomerGroup
     */
    public static putCustomerGroup=async (req:Request, res:Response)=>{
        let name=req.body.name;
        let id_customer_group=parseInt(req.params.id);
        let objCustomerGroup=new CustomerGroup();
        objCustomerGroup=await objCustomerGroup.findById(id_customer_group);
        if(objCustomerGroup.getExists()){
            objCustomerGroup.name=name;
            objCustomerGroup.insert().then(async status=>{
                if(status){
                    let element=await objCustomerGroup.toObject();
                    res.json({
                        status:true,
                        element
                    });
                }else{
                    res.json({
                        status:false,
                        error: 'Error'
                    });
                }
            });
        }else{
            res.json({
                status:false,
                error: 'not exists'
            });
        }
    }
    /**
     * Eliminar CustomerGroup
     */
    public static deleteCustomerGroup=async (req:Request, res:Response)=>{
        let id_customer_group=parseInt(req.params.id);
        let objCustomerGroup=new CustomerGroup();
        objCustomerGroup=await objCustomerGroup.findById(id_customer_group);
        if(objCustomerGroup.getExists()){
            objCustomerGroup.delete().then(async status=>{
                if(status){
                    res.json({
                        status:true
                    });
                }else{
                    res.json({
                        status:false,
                        error: 'Error'
                    });
                }
            });
        }else{
            res.json({
                status:false,
                error: 'not exists'
            });
        }
    }
}