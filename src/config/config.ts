class Config{
    private static _instance: Config;//propiedad de la instancia (Singleton)

    private config:any;
    constructor(){
        this.config=require('./config.json');
    }
    /**
     * devolver instancia (Singleton)
     */
    public static get instance(){
        return this._instance || (this._instance=new this());
    }
    public getConfig(){
        return this.config;
    }
}

let config=Config.instance.getConfig();

export default config;