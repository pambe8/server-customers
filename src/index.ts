import Server from './core/server';
import router from './router/router';
import config from './config/config';
import bodyParser = require('body-parser');
import cors from 'cors';
const helmet = require('helmet');

const server = Server.instance;

server.app.use(bodyParser.urlencoded({ extended: false }));
server.app.use(bodyParser.json());
server.app.use(cors(
    { 
        origin: '*',
        methods: 'GET, POST, PUT, DELETE, OPTIONS',
        optionsSuccessStatus: 200,
        credentials:true
    }
));
server.app.use(router);

server.app.use(helmet());

server.start(()=>{
    console.log(`Server started on port ${config.port}`);
});